//
//  FilterViewController.swift
//  Filter
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import UIKit

class FilterViewController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .cyan
        
        title = "Filter"
    }
    
    deinit {
        print("---------- FilterViewController deinit called -----------")
    }
}
