//
//  Assembly.swift
//  Filter
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import Foundation
import Swinject

public func initModule() -> Assembly { FilterAssembly() }

public class FilterAssembly: Assembly {
    public func assemble(container: Container) {
        
    }
}
