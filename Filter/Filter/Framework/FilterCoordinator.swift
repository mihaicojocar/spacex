//
//  FilterCoordinator.swift
//  Filter
//
//  Created by Mihai Liviu Cojocar on 05/05/2021.
//

import Foundation
import RxSwift
import Core

public class FilterCoordinator: BaseCoordinator<Void> {
    public var navigationController: UINavigationController?
    
    public override func start() -> Observable<Void> {
        let vc = FilterViewController()
//        navigationController?.present(vc, animated: true, completion: nil)
        navigationController?.pushViewController(vc, animated: true)
        return .empty()
    }
    
    deinit {
        print("=========== FilterCoordinator deinit called ============" )
    }
}
