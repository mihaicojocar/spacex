//
//  Assembly.swift
//  MissionDetails
//
//  Created by Mihai Liviu Cojocar on 01/05/2021.
//

import Swinject
import protocol Core.URLLaunching

public func initModule() -> Assembly { MissionDetailsAssembly() }

public class MissionDetailsAssembly: Assembly {
    public func assemble(container: Container) {
        container.register(MissionDetailsCoordinator.self) { resolver in
            let urlLauncher = resolver.resolve(URLLaunching.self)!
            let viewModel = MissionDetailsViewModel(urlLauncher: urlLauncher)
            return MissionDetailsCoordinator(viewModel: viewModel)
        }
    }
}
