//
//  MissionDetailsViewModel.swift
//  MissionDetails
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import Foundation
import RxSwift
import struct Core.Launch
import protocol Core.URLLaunching

public struct MissionDetailsViewModel {
    public var launch: Launch?
    public let openUrlSubject = PublishSubject<URL?>()
    
    private let urlLauncher: URLLaunching
    private let disposeBag = DisposeBag()
    
    public init(urlLauncher: URLLaunching) {
        self.urlLauncher = urlLauncher
        bindComponents()
    }
    
    private func bindComponents() {
        openUrlSubject
            .asObservable()
            .subscribe(onNext: { url in
                urlLauncher.open(url: url)
            })
            .disposed(by: disposeBag)
    }
}
