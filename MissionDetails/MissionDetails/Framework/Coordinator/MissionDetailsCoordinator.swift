//
//  MissionDetailsCoordinator.swift
//  MissionDetails
//
//  Created by Mihai Liviu Cojocar on 06/05/2021.
//

import Core
import RxSwift

public class MissionDetailsCoordinator: BaseCoordinator<Void> {
    public var viewModel: MissionDetailsViewModel
    public var navigationController: UINavigationController?
    
    init(viewModel: MissionDetailsViewModel) {
        self.viewModel = viewModel
    }
            
    public override func start() -> Observable<Void> {
        let viewController = MissionsDetailsViewController()
        viewController.viewModel = viewModel
        navigationController?.pushViewController(viewController, animated: true)
        
        return .empty()
    }
}
