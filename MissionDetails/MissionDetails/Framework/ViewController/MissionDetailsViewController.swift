//
//  MissionDetailsViewController.swift
//  MissionDetails
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

public class MissionsDetailsViewController: UIViewController {
    
    var viewModel: MissionDetailsViewModel?
    
    private let disposeBag = DisposeBag()
    
    private var articleButton: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.backgroundColor = .cyan
        button.setTitle("Click to open article...", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var wikipediaButton: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.backgroundColor = .cyan
        button.setTitle("Click to open wikipedia...", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var videoButton: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.backgroundColor = .cyan
        button.setTitle("Click to open video...", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 50
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindComponents()
    }
    
    private func bindComponents() {
        guard let viewModel = viewModel else { return }
        
        articleButton.rx.tap
            .map { viewModel.launch?.links.article }
            .bind(to: viewModel.openUrlSubject)
            .disposed(by: disposeBag)
        
        wikipediaButton.rx.tap
            .map { viewModel.launch?.links.wikipedia }
            .bind(to: viewModel.openUrlSubject)
            .disposed(by: disposeBag)
        
        videoButton.rx.tap
            .map { viewModel.launch?.links.video }
            .bind(to: viewModel.openUrlSubject)
            .disposed(by: disposeBag)
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        guard let links = viewModel?.launch?.links else { return }
        
        if links.article != nil {
            contentStackView.addArrangedSubview(articleButton)
        }
        if links.wikipedia != nil {
            contentStackView.addArrangedSubview(wikipediaButton)
        }
        if links.video != nil {
            contentStackView.addArrangedSubview(videoButton)
        }
        
        view.addSubview(contentStackView)
        
        contentStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().inset(100)
            make.height.equalTo(200)
        }
    }
}

