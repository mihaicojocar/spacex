//
//  AppDelegate.swift
//  SpaceX
//
//  Created by Mihai Liviu Cojocar on 01/05/2021.
//

import UIKit
import Missions
import RxSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let disposeBag = DisposeBag()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let coordinator = AppCoordinator(window: window!)
        
        coordinator.start()
            .subscribe()
            .disposed(by: disposeBag)
        
        return true
    }
}

