//
//  AppCoordinator.swift
//  SpaceX
//
//  Created by Mihai Liviu Cojocar on 03/05/2021.
//

import UIKit
import Core
import RxSwift
import Missions

class AppCoordinator: BaseCoordinator<Void> {

    private let window: UIWindow
    private let dependencies: Coordinators

    init(window: UIWindow) {
        self.window = window
        self.dependencies = Dependencies()
    }

    override func start() -> Observable<Void> {
        guard let missionsCoordinator = dependencies.missionsCoordinator,
              let viewController = missionsCoordinator.viewController else { return .empty() }
        
        let navigationController = UINavigationController(rootViewController: viewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()

        return coordinate(to: missionsCoordinator)
    }
}
