//
//  Dependencies.swift
//  SpaceX
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import Foundation
import Swinject
import Core
import Missions
import MissionDetails

class Dependencies {
    private let container: Container
    
    private let coreAssembly: Assembly
    private let missionsAssembly: Assembly
    private let missionDetailsAssembly: Assembly
    
    init() {
        self.container = Container()
        
        coreAssembly = Core.initModule()
        missionsAssembly = Missions.initModule()
        missionDetailsAssembly = MissionDetails.initModule()
        
        coreAssembly.assemble(container: container)
        missionsAssembly.assemble(container: container)
        missionDetailsAssembly.assemble(container: container)
    }
}

protocol ViewControllers {
    var missionsViewController: MissionsViewController? { get }
}

protocol Coordinators {
    var missionsCoordinator: MissionsCoordinator? { get }
}

extension Dependencies: ViewControllers {
    var missionsViewController: MissionsViewController? {
        return container.resolve(MissionsViewController.self)
    }
}

extension Dependencies: Coordinators {
    var missionsCoordinator: MissionsCoordinator? {
        return container.resolve(MissionsCoordinator.self)
    }
}
