//
//  Int+InBillions.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import Foundation

public extension Int {
    func inBillions() -> Double {
        return Double(self) / 1000000000
    }
}
