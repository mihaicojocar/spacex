//
//  Date+From_en_US_POSIX.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 05/05/2021.
//

import Foundation

public extension Date {
    static func from_en_US_POSIX(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.date(from: string)
    }
    
    func formatDateToDayMonthYearString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: self)
    }
    
    func formatDateToDayTimeString() -> String {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self)
        let minutes = calendar.component(.minute, from: self)
        var minutesString = String(describing: minutes)
        if minutes < 10 {
            minutesString = "0\(minutes)"
        }
        return "\(hour):\(minutesString)"
    }
    
    func isInPast() -> Bool {
        return self < Date()
    }
    
    func daysFromNow() -> Int? {
        Calendar.current.dateComponents([.day], from: Date(), to: self).day
    }
    
    func year() -> Int {
        let calendar = Calendar.current
        return calendar.component(.year, from: self)
    }
}
