//
//  UIBarButtonItem.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 05/05/2021.
//

import UIKit

public extension UIBarButtonItem {
    static func using(_ button: UIButton) -> UIBarButtonItem {
        let menuBarItem = UIBarButtonItem(customView: button)
        menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24).isActive = true
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        return menuBarItem
    }
}
