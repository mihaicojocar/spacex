//
//  Launch.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import Foundation

public struct Rocket: Decodable {
    public let id: String
    public let name: String
    public let type: String
    
    enum CodingKeys: String, CodingKey {
        case id = "rocket_id"
        case name = "rocket_name"
        case type = "rocket_type"
    }
    
    public var description: String {
        return "\(name) / \(type)"
    }
}

public struct Links: Decodable {
    public let missionPatch: URL?
    public let article: URL?
    public let wikipedia: URL?
    public let video: URL?
    
    enum CodingKeys: String, CodingKey {
        case missionPatch = "mission_patch_small"
        case video = "video_link"
        case article = "article_link"
        case wikipedia
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.missionPatch = URL(string: try (container.decodeIfPresent(String.self, forKey: .missionPatch) ?? ""))
        self.article = URL(string: try (container.decodeIfPresent(String.self, forKey: .article) ?? ""))
        self.wikipedia = URL(string: try (container.decodeIfPresent(String.self, forKey: .wikipedia) ?? ""))
        self.video = URL(string: try (container.decodeIfPresent(String.self, forKey: .video) ?? ""))
    }
}

public struct Launch: Decodable {
    public let missionName: String
    public let launchSuccess: Bool?
    public let upcoming: Bool
    public let dateUnix: TimeInterval
    public let date: Date?
    public let rocket: Rocket
    public let links: Links
    
    enum CodingKeys: String, CodingKey {
        case missionName = "mission_name"
        case launchSuccess = "launch_success"
        case dateUnix = "launch_date_unix"
        case date = "launch_date_utc"
        case upcoming, rocket, links
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.missionName = try container.decode(String.self, forKey: .missionName)
        self.launchSuccess = try container.decodeIfPresent(Bool.self, forKey: .launchSuccess)
        self.upcoming = try container.decode(Bool.self, forKey: .upcoming)
        self.dateUnix = try container.decode(TimeInterval.self, forKey: .dateUnix)
        self.rocket = try container.decode(Rocket.self, forKey: .rocket)
        self.links = try container.decode(Links.self, forKey: .links)
        
        let dateString = try container.decode(String.self, forKey: .date)
        self.date = Date.from_en_US_POSIX(string: dateString)
    }
    
    public func formatedDate() -> String {
        return (date?.formatDateToDayMonthYearString() ?? "")
            + " at "
            + (date?.formatDateToDayTimeString() ?? "")
    }
}
