//
//  Filter.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import Foundation

public struct Filter {
    public let ascending: Bool
    public let launchYear: Int?
    public let launched: Bool?
    
    public static func from(searchText: String?, scope: String?) -> Self {
        var isAscending = true
        var isLaunched: Bool?
        var launchYear: Int?
        
        if let scope = scope {
            switch scope {
            case "Ascending":
                isAscending = true
                isLaunched = nil
            case "Descending":
                isAscending = false
                isLaunched = nil
            case "Launched":
                isAscending = true
                isLaunched = true
            case "Failed":
                isAscending = true
                isLaunched = false
            default: break
            }
        }

        if let launchYearString = searchText, !launchYearString.isEmpty {
            launchYear = Int(launchYearString)
        }
        
        return .init(ascending: isAscending, launchYear: launchYear, launched: isLaunched)
    }
    
    public static func initialValue() -> Self {
        .init(ascending: true, launchYear: nil, launched: nil)
    }
}

extension Filter: Equatable {}
