//
//  CompanyInfo.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import Foundation

public struct CompanyInfo: Decodable {
    public let name: String
    public let founder: String
    public let founded: Int
    public let employees: Int
    public let launchSites: Int
    public let valuation: Int
    
    enum CodingKeys: String, CodingKey {
        case name, founder, founded, employees, valuation
        case launchSites = "launch_sites"
    }
}
