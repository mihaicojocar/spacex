//
//  Assembly.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 01/05/2021.
//

import Swinject

public func initModule() -> Assembly { CoreAssembly() }

public class CoreAssembly: Assembly {
    public func assemble(container: Container) {
        container.register(Networking.self) { _ in
            NetworkService()
        }.inObjectScope(.container)
        
        container.register(MissionsProvider.self) { resolver in
            let networkService = resolver.resolve(Networking.self)!
            return MissionsService(networkClient: networkService)
        }
        
        container.register(URLLaunching.self) { _ in
            URLLauncher()
        }.inObjectScope(.container)
    }
}
