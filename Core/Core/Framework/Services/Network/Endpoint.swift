//
//  Endpoint.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 03/05/2021.
//

import Foundation

typealias Path = String
typealias Parameters = [String: String]

public enum Method: String {
    case get = "GET"
}

public struct Endpoint<Response> {
    let method: Method
    let baseURL: URL
    let path: Path
    var parameters: Parameters
    var body: Parameters
    var query: Parameters
    let decode: (Data) throws -> Response
    
    init(
        method: Method,
        baseURL: URL,
        path: Path,
        parameters: Parameters,
        body: Parameters,
        query: Parameters,
        decode: @escaping (Data) throws -> Response
    ) {
        self.method = method
        self.baseURL = baseURL
        self.path = path
        self.parameters = parameters
        self.body = body
        self.query = query
        self.decode = decode
    }
}

extension Endpoint where Response: Decodable {
    init(
        method: Method = .get,
        baseURL: URL = URL(string: "https://api.spacexdata.com")!,
        path: Path,
        parameters: Parameters = [:],
        body: Parameters = [:],
        query: Parameters = [:]
    ) {
        self.init(
            method: method,
            baseURL: baseURL,
            path: path,
            parameters: parameters,
            body: body,
            query: query,
            decode: { data in
                try JSONDecoder().decode(Response.self, from: data)
            }
        )
    }
}

extension Endpoint {
    func requestBody(using encoding: String.Encoding = .utf8) -> Data? {
        let parametersArray = self.body.map { "\($0.key)=\($0.value)" }
        return parametersArray.joined(separator: "&").data(using: encoding)
    }
}

