//
//  NetworkService.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 03/05/2021.
//

import RxSwift
import RxCocoa

public protocol ReactiveSession {
    func rx_response(request: URLRequest) -> Observable<(response: HTTPURLResponse, data: Data)>
}

extension URLSession: ReactiveSession {
    public func rx_response(request: URLRequest) -> Observable<(response: HTTPURLResponse, data: Data)> {
        return self.rx.response(request: request)
    }
}


public protocol Networking {
    func request<Response>(_ endpoint: Endpoint<Response>) -> Single<(response: HTTPURLResponse, result: Response)>
}

enum NetworkError: Error {
    case serverError(statusCode: Int)
    case invalidURL
    case invalidJSON(Error)
    case unknown
}


public final class NetworkService: Networking {
    private let session: ReactiveSession
    private let requestTimeOut = TimeInterval(20)
    
    public init(session: ReactiveSession = URLSession(configuration: .default)) {
        self.session = session
    }
    
    public func request<Response>(_ endpoint: Endpoint<Response>) -> Single<(response: HTTPURLResponse, result: Response)> {
        var urlComponents = URLComponents(url: endpoint.baseURL, resolvingAgainstBaseURL: false)
        urlComponents?.path = endpoint.path
        let queryItems = endpoint.query.map { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponents?.queryItems = queryItems
        
        guard let url = urlComponents?.url else {
            return Single.error(NetworkError.invalidURL)
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue
        request.timeoutInterval = requestTimeOut
        
        request.allHTTPHeaderFields = endpoint.parameters
        request.httpBody = endpoint.requestBody()
        
        return session
            .rx_response(request: request)
            .map { (response, data) -> (HTTPURLResponse, Response) in
                guard (200...299).contains(response.statusCode) else {
                    throw NetworkError.serverError(statusCode: response.statusCode)
                }
                
                let result = try endpoint.decode(data)
                return (response: response, result: result)
            }
            .asSingle()
    }
}
