//
//  URLLauncher.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import UIKit

public protocol URLLaunching {
    func open(url: URL?)
}

public class URLLauncher: URLLaunching {
    public func open(url: URL?) {
        guard let url = url else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
