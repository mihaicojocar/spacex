//
//  MissionsService.swift
//  Core
//
//  Created by Mihai Liviu Cojocar on 03/05/2021.
//

import RxSwift

public protocol MissionsProvider {
    func companyInfo() -> Single<CompanyInfo>
    func launches() -> Single<[Launch]>
}

public class MissionsService: MissionsProvider {
    private let networkClient: Networking
    
    init(networkClient: Networking) {
        self.networkClient = networkClient
    }
    
    public func companyInfo() -> Single<CompanyInfo> {
        let endpoint = Endpoint<CompanyInfo>(path: "/v3/info")
        return request(endpoint)
    }
    
    public func launches() -> Single<[Launch]> {
        let endpoint = Endpoint<[Launch]>(path: "/v3/launches")
        return request(endpoint)
    }
    
    
    private func request<Result>(_ endpoint: Endpoint<Result>) -> Single<Result> {
        return networkClient.request(endpoint).map { (response, result) -> Result in
            return result
        }
    }
}
