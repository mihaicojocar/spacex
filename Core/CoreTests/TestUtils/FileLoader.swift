//
//  FileLoader.swift
//  CoreTests
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import Foundation

typealias JSON = [String: Any]

/// Helper class to load data from the filesystem
class FileLoader {
    
    /// Loads a json file from the filesystem and tries
    /// to parse it into a JSON object and return it.
    ///
    /// - Parameter filename: The name of the file without extension
    /// - Returns: A JSON object if it exists.
    func loadJson(from filename: String) -> JSON? {
        guard let data = loadData(from: filename) else {
            return nil
        }
        return try? JSONSerialization.jsonObject(with: data, options: []) as? JSON
    }
    
    /// Loads a file from the filesystem (json by default) and tries
    /// to store it's contents on a Data object.
    ///
    /// - Parameters:
    ///   - filename: The name of the file without the extension
    ///   - ofType: The extension of the file, json by default.
    /// - Returns: A Data object if possible.
    func loadData(from filename: String, ofType: String = "json") -> Data? {
        let bundle = Bundle(for: type(of: self))
        let pathCandidate = bundle.path(forResource: filename, ofType: ofType)
        guard let path = pathCandidate else {
            return nil
        }
        return try? Data(contentsOf: URL(fileURLWithPath: path))
    }
}
