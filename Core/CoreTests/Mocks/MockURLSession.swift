//
//  MockURLSession.swift
//  CoreTests
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import Foundation
import RxSwift
@testable import Core

class MockURLSession: ReactiveSession {
    private var response: HTTPURLResponse
    private var data: Data
    
    init(response: HTTPURLResponse, data: Data) {
        self.response = response
        self.data = data
    }
    
    func rx_response(request: URLRequest) -> Observable<(response: HTTPURLResponse, data: Data)> {
        return Observable.just((response: self.response, data: self.data))
    }
}
