//
//  NetworkServiceTests.swift
//  CoreTests
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import RxSwift
import RxBlocking
import Quick
import Nimble
@testable import Core

class NetworkServiceTests: QuickSpec {
    override func spec() {
        describe("Using NetworkService") {
            let url = URL(string: "https://api.spacexdata.com/")!
            var sut: Networking!
            var response: HTTPURLResponse!
            var sessionMock: MockURLSession!
            let fileLoader = FileLoader()
            let endpoint = Endpoint<TestObject>(path: "/some_path")
            
            describe("when the response status code is in the 200...299 range") {
                describe("when the json is valid and all keys are present") {
                    beforeEach {
                        response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                        let data = fileLoader.loadData(from: "success", ofType: "json")!
                        sessionMock = MockURLSession(response: response, data: data)
                        sut = NetworkService(session: sessionMock)
                    }
                    
                    afterEach {
                        response = nil
                    }
                    
                    it("emits a single success containing the response and the decoded data") {
                        let result = try? sut.request(endpoint).toBlocking().first()
                        
                        if let result = result {
                            expect(result.response).to(equal(response))
                            expect(result.result.title).to(equal("Test object title"))
                        } else {
                            fail("Should be success")
                        }
                    }
                }
                
                describe("when the json is malformed") {
                    beforeEach {
                        response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                        let data = fileLoader.loadData(from: "invalidJSON", ofType: "json")!
                        sessionMock = MockURLSession(response: response, data: data)
                        sut = NetworkService(session: sessionMock)
                    }
                    
                    afterEach {
                        response = nil
                    }
                    
                    it("emits a decoding error") {
                        do {
                            _ = try sut.request(endpoint).toBlocking().first()
                        } catch {
                            if case DecodingError.dataCorrupted = error {} else {
                                fail("Should fail with decoding error error.")
                            }
                        }
                    }
                }
                
                describe("when a key is missing from the json response") {
                    beforeEach {
                        response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                        let data = fileLoader.loadData(from: "missingKey", ofType: "json")!
                        sessionMock = MockURLSession(response: response, data: data)
                        sut = NetworkService(session: sessionMock)
                    }
                    
                    afterEach {
                        response = nil
                    }
                    
                    it("emits a decoding error") {
                        do {
                            _ = try sut.request(endpoint).toBlocking().first()
                        } catch {
                            if case DecodingError.keyNotFound = error {} else {
                                fail("Should fail with decoding error error.")
                            }
                        }
                    }
                }
            }
            
            describe("when the URL is invalid") {
                beforeEach {
                    response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
                    let data = fileLoader.loadData(from: "success", ofType: "json")!
                    sessionMock = MockURLSession(response: response, data: data)
                    sut = NetworkService(session: sessionMock)
                }
                
                afterEach {
                    response = nil
                }
                
                it("emits an invalidURL error") {
                    let endpoint = Endpoint<TestObject>(baseURL: url, path: "some invalid path")
                    do {
                        _ = try sut.request(endpoint).toBlocking().first()
                    } catch {
                        if case NetworkError.invalidURL = error {} else {
                            fail("Should fail with invalidURL error.")
                        }
                    }
                }
            }
            
            describe("when the response status code is not in the 200...299 range") {
                beforeEach {
                    response = HTTPURLResponse(url: url, statusCode: 404, httpVersion: nil, headerFields: nil)
                    let data = fileLoader.loadData(from: "success", ofType: "json")!
                    sessionMock = MockURLSession(response: response, data: data)
                    sut = NetworkService(session: sessionMock)
                }
                
                afterEach {
                    response = nil
                }
                
                it("emits a server error with the status code") {
                    do {
                        _ = try sut.request(endpoint).toBlocking().first()
                    } catch {
                        if case let NetworkError.serverError(statusCode: statusCode) = error {
                            expect(statusCode).to(equal(404))
                        } else {
                            fail("Should fail with decoding error error.")
                        }
                    }
                }
            }
        }
    }
}
