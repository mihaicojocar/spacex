# SpaceX

## Installation
Clone repo: `git clone git@bitbucket.org:mihaicojocar/spacex.git`

Install dependencies: `pod install`

## Dependencies
* [Swinject](https://github.com/Swinject/Swinject)
* [RxSwift](https://github.com/ReactiveX/RxSwift)
* [RxDatasources](https://github.com/RxSwiftCommunity/RxDataSources)
* [SnapKit](https://github.com/SnapKit/SnapKit)
* [SDWebImage](https://github.com/SDWebImage/SDWebImage)

## Architecture
* Reactive MVVM with Coordinators.
* Modularized app (each module is build as a framework and `Swinject` was used to pass dependencies between modules).


**NOTE:**
Unfortunately, due to time constraints (being very busy at work), I had to cut some corners and there are missing unit tests. The dependencies are injected and conform to protocols, giving the opportunity to be replaced by mocks and objects can be tested in isolation. I could add the missing tests over the weekend, if possible.

## Author

MihaiLiviuCojocar, cmihailiviu@yahoo.com

## License

SpaceX is available under the MIT license. See the LICENSE file for more info.
