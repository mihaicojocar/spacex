//
//  Assembly.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 01/05/2021.
//

import Swinject
import Core
import MissionDetails

public func initModule() -> Assembly { MissionsAssembly() }

public class MissionsAssembly: Assembly {
    public func assemble(container: Container) {
        container.register(MissionsViewModel.self) { resolver in
            let missionsService = resolver.resolve(MissionsProvider.self)!
            return MissionsViewModel(missionsService: missionsService)
        }.inObjectScope(.container)
        
        container.register(MissionsViewController.self) { resolver in
            let viewModel = resolver.resolve(MissionsViewModel.self)
            let viewController = MissionsViewController()
            viewController.viewModel = viewModel
            return viewController
        }.inObjectScope(.container)
        
        container.register(MissionsCoordinator.self) { resolver in
            let viewController = resolver.resolve(MissionsViewController.self)
            let viewModel = resolver.resolve(MissionsViewModel.self)
            let missionDetailsCoordinator = resolver.resolve(MissionDetailsCoordinator.self)
                let coordinator = MissionsCoordinator(
                    viewController: viewController,
                    viewModel: viewModel,
                    missionDetailsCoordinator: missionDetailsCoordinator
                )
            return coordinator
        }.inObjectScope(.container)
    }
}
