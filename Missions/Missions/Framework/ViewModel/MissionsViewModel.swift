//
//  MissionsViewModel.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 03/05/2021.
//

import RxSwift
import Core

public class MissionsViewModel {
    // MARK: - Dependencies
    private let missionsService: MissionsProvider
    
    // MARK: - Private properties
    private let disposeBag = DisposeBag()
    private let globalScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    private let sectionsObservable = PublishSubject<[MultipleSectionModel]>()
    private let filteredDataSubject = PublishSubject<[MultipleSectionModel]>()
    
    private lazy var companyInfoObservable: Observable<MultipleSectionModel> = {
        return missionsService
            .companyInfo()
            .asObservable()
            .map { companyInfo -> MultipleSectionModel in
                let companySection = MultipleSectionModel.company(
                    title: "COMPANY",
                    items: [SectionItem.companySectionItem(company: companyInfo)]
                )
                return companySection
            }
    }()
    
    private lazy var launchesObservable: Observable<MultipleSectionModel> = {
        return missionsService
            .launches()
            .asObservable()
            .map { launches -> MultipleSectionModel in
                let launchesSection = MultipleSectionModel.launches(
                    title: "LAUNCHES",
                    items: launches.map {SectionItem.launchSectionItem(launch: $0)}
                )
                return launchesSection
            }
    }()
    
    private lazy var mergedCompanyInfoAndLaunchesObservable: Observable<[MultipleSectionModel]> = {
        return Observable.merge(companyInfoObservable, launchesObservable)
            .scan([]) { acc, sectionModel -> [MultipleSectionModel] in
                var accumulator = acc
                let newSection = sectionModel
                accumulator.append(newSection)
                accumulator.sort(by: { $0.title < $1.title })
                return accumulator
            }
    }()
    
    // MARK: Out
    var data: Observable<[MultipleSectionModel]> {
        return filteredDataSubject
            .asObservable()
            .startWith(
                [MultipleSectionModel.loadingSection(title: "COMPANY"),
                 MultipleSectionModel.loadingSection(title: "LAUNCHES")]
            )        
    }
    
    // MARK: - In
    let selectedLaunch = PublishSubject<Launch>()
    let filterSubject = BehaviorSubject<Filter>(value: Filter.initialValue())
    
    // MARK: - Init
    init(missionsService: MissionsProvider) {
        self.missionsService = missionsService
        
        bindComponents()
    }
    
    // MARK: - Public
    func fetchData() {
        mergedCompanyInfoAndLaunchesObservable
            .subscribe(
                onNext: { [weak self] sections in
                    self?.sectionsObservable.onNext(sections)
                },
                onError: { error in
                    print(error)
                }
            )
            .disposed(by: disposeBag)
    }
    
    // MARK: - Private
    private func bindComponents() {
        let filter = filterSubject
            .asObservable()
            .observe(on: globalScheduler)
            .distinctUntilChanged()
            .debounce(.milliseconds(500), scheduler: globalScheduler)
            .map { $0 as AnyObject }
        
        let unfilteredData = sectionsObservable.map { $0 as AnyObject }
        
        let filterAndDataObservable = Observable.combineLatest(filter, unfilteredData) { ($0, $1) }
        
        filterAndDataObservable
            .subscribe { [weak self] (filter, data) in
                guard let filter = filter as? Filter, let data = data as? [MultipleSectionModel] else {
                    return
                }
                var filteredData = data
                if let launchesSectionModelIndex = data.firstIndex(where: { $0.title == "LAUNCHES" }) {
                    let blah = filteredData.remove(at: launchesSectionModelIndex)
                    filteredData.insert(
                        blah.sortedLaunches(
                            ascending: filter.ascending,
                            launchSuccessful: filter.launched,
                            launchYear: filter.launchYear),
                        at: launchesSectionModelIndex
                    )
                }
                
                self?.filteredDataSubject.onNext(filteredData)
            }
            .disposed(by: disposeBag)
    }
}
