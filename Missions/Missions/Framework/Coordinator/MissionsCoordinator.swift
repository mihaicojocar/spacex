//
//  MissionsCoordinator.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import Foundation
import RxSwift
import Core
import MissionDetails

public class MissionsCoordinator: BaseCoordinator<Void> {
    public var viewController: MissionsViewController?
    private var viewModel: MissionsViewModel?
    private var missionDetailsCoordinator: MissionDetailsCoordinator?
    
    public init(
        viewController: MissionsViewController?,
        viewModel: MissionsViewModel?,
        missionDetailsCoordinator: MissionDetailsCoordinator?) {
        self.viewController = viewController
        self.viewModel = viewModel
        self.missionDetailsCoordinator = missionDetailsCoordinator
    }
    
    public override func start() -> Observable<Void> {
        viewModel?.selectedLaunch
            .subscribe(onNext: { [weak self] launch in
                self?.coordinateToLaunch(launch)
            })
            .disposed(by: disposeBag)
        
        return .never()
    }
    
    @discardableResult
    private func coordinateToLaunch(_ launch: Launch) -> Observable<Void> {
        guard let missionDetailsCoordinator = missionDetailsCoordinator else { return .empty() }
        
        missionDetailsCoordinator.navigationController = viewController?.navigationController
        missionDetailsCoordinator.viewModel.launch = launch
        return missionDetailsCoordinator.start()
    }
}
