//
//  MissionsViewController.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 02/05/2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources
import Core

public class MissionsViewController: UIViewController {
    // MARK: - ViewModel
    var viewModel: MissionsViewModel?
    
    // MARK: - Private properties
    private let disposeBag = DisposeBag()
    
    // MARK: - UI elements
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
        tableView.backgroundColor = .systemBackground
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "filter"), for: .normal)
        button.tintColor = .systemGray
        return button
    }()
    
    private lazy var filterController: UISearchController = {
        let controller = UISearchController()
        controller.searchResultsUpdater = self
        controller.searchBar.delegate = self
        controller.searchBar.scopeButtonTitles = ["Ascending", "Descending", "Launched", "Failed"]
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.searchTextField.placeholder = "Filter by year..."
        controller.searchBar.returnKeyType = .done
        return controller
    }()
    
    // MARK: - ViewController lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        registerTableViewCells()
        bindComponents()
        
        viewModel?.fetchData()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        title = "SpaceX"
    }
    
    // MARK: - Private
    
    // MARK: - UI
    private func setupUI() {
        view.addSubview(tableView)
        navigationItem.rightBarButtonItem = UIBarButtonItem.using(filterButton)
    }
    
    private func registerTableViewCells() {
        tableView.register(CompanyInfoCell.self, forCellReuseIdentifier: CompanyInfoCell.reuseIdentifier)
        tableView.register(LaunchCell.self, forCellReuseIdentifier: LaunchCell.reuseIdentifier)
        tableView.register(LoadingCell.self, forCellReuseIdentifier: LoadingCell.reuseIdentifier)
    }
    
    private func toggleSearchController(isEnabled: Bool) {
        if navigationItem.searchController == nil {
            navigationItem.searchController = filterController
            filterController.loadViewIfNeeded()
            navigationController?.view.setNeedsLayout()
            navigationController?.view.layoutSubviews()
        }
        navigationItem.searchController?.isActive = isEnabled
    }
    
    // MARK: - Data binding
    private func bindComponents() {
        guard let viewModel = viewModel else { return }
        
        filterButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.toggleSearchController(isEnabled: true)
            })
            .disposed(by: disposeBag)
        
        let dataSource = MissionsViewController.dataSource()
        
        viewModel.data
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        tableView.rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                if case let .launchSectionItem(launch: launch) = dataSource[indexPath] {
                    viewModel.selectedLaunch.onNext(launch)
                    self?.tableView.deselectRow(at: indexPath, animated: true)
                }
            })
            .disposed(by: disposeBag)
            
    }
}
