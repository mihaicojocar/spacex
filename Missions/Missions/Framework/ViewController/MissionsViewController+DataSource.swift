//
//  MissionsViewController+DataSource.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import RxDataSources

extension MissionsViewController {
    static func dataSource() -> RxTableViewSectionedReloadDataSource<MultipleSectionModel> {
        return RxTableViewSectionedReloadDataSource<MultipleSectionModel>(
            configureCell: { dataSource, table, idxPath, item in
                switch dataSource[idxPath] {
                case let .companySectionItem(company: company):
                    let cell: CompanyInfoCell = table.dequeueReusableCell(forIndexPath: idxPath)
                    cell.configure(with: company)
                    return cell
                case let .launchSectionItem(launch: launch):
                    let cell: LaunchCell = table.dequeueReusableCell(forIndexPath: idxPath)
                    cell.configure(with: launch)
                    return cell
                case .loading:
                    let cell: LoadingCell = table.dequeueReusableCell(forIndexPath: idxPath)
                    return cell
                }
            },
            titleForHeaderInSection: { dataSource, index in
                let section = dataSource[index]
                return section.title
            }
        )
    }
}
