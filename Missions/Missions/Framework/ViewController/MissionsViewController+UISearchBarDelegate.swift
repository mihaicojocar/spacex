//
//  MissionsViewController+UISearchBarDelegate.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import UIKit
import struct Core.Filter

extension MissionsViewController: UISearchResultsUpdating, UISearchBarDelegate {
    public func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scopeButton = searchBar.scopeButtonTitles?[searchBar.selectedScopeButtonIndex]
        let searchText = searchBar.text
        
        let filter = Filter.from(searchText: searchText, scope: scopeButton)
        
        viewModel?.filterSubject.onNext(filter)
    }
}
