//
//  LaunchLoadingCell.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

 import UIKit

public class LaunchLoadingCell: UITableViewCell {
    // MARK: - UI elements
    private lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Init
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    private func configureUI() {
        
    }
}
