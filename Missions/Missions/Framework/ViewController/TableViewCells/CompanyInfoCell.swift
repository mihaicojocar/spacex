//
//  CompanyInfoCell.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import UIKit
import SnapKit
import Core

public class CompanyInfoCell: UITableViewCell {
    // MARK: - UI elements
    private lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Init
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public APIs
    public func configure(with companyInfo: CompanyInfo) {
        label.text = "\(companyInfo.name) was founded by \(companyInfo.founder) in \(companyInfo.founded). It has now \(companyInfo.employees) employees, \(companyInfo.launchSites) launch sites, and is valued at USD \(companyInfo.valuation.inBillions()) bl."
    }
    
    
    // MARK: - Private
    private func configureUI() {
        contentView.backgroundColor = .systemGray6
        
        addSubview(label)
        
        label.snp.makeConstraints { make in
            make.top.left.equalToSuperview().offset(20)
            make.right.bottom.equalToSuperview().offset(-20)
        }
    }
}
