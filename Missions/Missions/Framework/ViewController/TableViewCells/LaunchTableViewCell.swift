//
//  LaunchCell.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import UIKit
import SnapKit
import Core
import SDWebImage

public class LaunchCell: UITableViewCell {
    // MARK: - UIElements
    private lazy var mainContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemGray6
        view.layer.cornerRadius = 5
        return view
    }()
    
    private lazy var missionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var successfulLaunchImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .clear
        imageView.sizeToFit()
        return imageView
    }()
    
    private lazy var missionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Mission:"
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        return label
    }()
    
    private lazy var missionNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = false
        UIFont.systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    let dateTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Date/time:"
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        return label
    }()
    
    let dateTimeDetailsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = false
        UIFont.systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    let rocketLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Rocket:"
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        return label
    }()
    
    let rocketNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = false
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    let daysLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = "Days from now:"
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        return label
    }()
    
    let daysDetailsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = false
        UIFont.systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    // MARK: - Init
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public APIs
    public func configure(with launch: Launch) {
        missionImageView.sd_setImage(with: launch.links.missionPatch, completed: nil)
        if let launchSuccess = launch.launchSuccess {
            let image = launchSuccess ? UIImage(named: "success") : UIImage(named: "fail")
            successfulLaunchImageView.image = image
        }
        
        missionNameLabel.text = launch.missionName
        dateTimeDetailsLabel.text = launch.formatedDate()
        rocketNameLabel.text = launch.rocket.description
        let blah = (launch.date?.isInPast() ?? false) ? "since" : "from"
        let days = abs(launch.date?.daysFromNow() ?? 0)
        daysLabel.text = "Days \(blah) now:"
        daysDetailsLabel.text = String(describing: days)
        
        layoutIfNeeded()
    }
    
    public override func prepareForReuse() {
        missionNameLabel.text = nil
        dateTimeDetailsLabel.text = nil
        rocketNameLabel.text = nil
        daysDetailsLabel.text = nil
        missionImageView.image = nil
        successfulLaunchImageView.image = nil
        
        super.prepareForReuse()
    }
    
    // MARK: - Private
    
    // MARK: Setup views
    private func configureUI() {
        addSubview(mainContentView)
        mainContentView.addSubview(missionImageView)
        mainContentView.addSubview(missionLabel)
        mainContentView.addSubview(missionNameLabel)
        mainContentView.addSubview(dateTimeLabel)
        mainContentView.addSubview(dateTimeDetailsLabel)
        mainContentView.addSubview(rocketLabel)
        mainContentView.addSubview(rocketNameLabel)
        mainContentView.addSubview(daysLabel)
        mainContentView.addSubview(daysDetailsLabel)
        mainContentView.addSubview(successfulLaunchImageView)
        
        setupConstraints()
    }
    
    // MARK: Setup constraints
    private func setupConstraints() {
        mainContentView.snp.makeConstraints { make in
            make.top.left.equalToSuperview().offset(2)
            make.bottom.right.equalToSuperview().offset(-2)
        }
        
        missionImageView.snp.makeConstraints { make in
            make.width.equalTo(30)
            make.height.equalTo(30)
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview()
        }
        
        successfulLaunchImageView.snp.makeConstraints { make in
            make.width.equalTo(missionImageView.snp.width)
            make.height.equalTo(missionImageView.snp.height)
            make.top.equalTo(missionImageView.snp.top)
            make.right.equalToSuperview().offset(-10)
        }
        
        // MARK: - Right labels
        
        missionNameLabel.snp.makeConstraints { make in
            make.top.equalTo(missionImageView.snp.top)
            make.right.equalTo(successfulLaunchImageView.snp.left).offset(-5)
            make.left.equalTo(daysDetailsLabel.snp.left)
            make.bottom.equalTo(dateTimeDetailsLabel.snp.top).offset(-5)
        }
        
        dateTimeDetailsLabel.snp.makeConstraints { make in
            make.right.equalTo(missionNameLabel.snp.right)
            make.left.equalTo(daysDetailsLabel.snp.left)
            make.bottom.equalTo(rocketNameLabel.snp.top).offset(-5)
        }
        
        rocketNameLabel.snp.makeConstraints { make in
            make.right.equalTo(missionNameLabel.snp.right)
            make.left.equalTo(daysDetailsLabel.snp.left)
            make.bottom.equalTo(daysDetailsLabel.snp.top).offset(-5)
        }
        
        daysDetailsLabel.snp.makeConstraints { make in
            make.right.equalTo(missionNameLabel.snp.right)
            make.bottom.equalToSuperview().offset(-10)
            make.left.equalTo(daysLabel.snp.right).offset(10)
        }
        
        // MARK: - Left labels
        
        missionLabel.snp.makeConstraints { make in
            make.left.equalTo(missionImageView.snp.right).offset(5)
            make.top.equalTo(missionNameLabel.snp.top)
        }
        
        dateTimeLabel.snp.makeConstraints { make in
            make.left.equalTo(missionLabel.snp.left)
            make.top.equalTo(dateTimeDetailsLabel.snp.top)
        }
        
        rocketLabel.snp.makeConstraints { make in
            make.left.equalTo(missionLabel.snp.left)
            make.top.equalTo(rocketNameLabel.snp.top)
        }
        
        daysLabel.snp.makeConstraints { make in
            make.left.equalTo(missionLabel.snp.left)
            make.top.equalTo(daysDetailsLabel.snp.top)
            make.width.equalTo(110)
        }
    }
}

