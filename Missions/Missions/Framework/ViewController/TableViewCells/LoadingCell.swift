//
//  CompanyInfoLoadingCell.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import UIKit
import SnapKit

public class LoadingCell: UITableViewCell {
    // MARK: - UI elements
    private lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Loading data..."
        return label
    }()
    
    // MARK: - Init
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    private func configureUI() {
        addSubview(label)
        label.snp.makeConstraints { make in
            make.width.equalToSuperview().inset(20)
            make.height.equalTo(100)
            make.top.bottom.equalToSuperview()
        }
    }
}
