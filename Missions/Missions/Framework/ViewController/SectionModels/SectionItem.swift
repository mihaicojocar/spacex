//
//  SectionItem.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 07/05/2021.
//

import Foundation
import struct Core.CompanyInfo
import struct Core.Launch

enum SectionItem {
    case companySectionItem(company: CompanyInfo)
    case launchSectionItem(launch: Launch)
    case loading
}
