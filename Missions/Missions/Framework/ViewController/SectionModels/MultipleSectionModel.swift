//
//  MultipleSectionModel.swift
//  Missions
//
//  Created by Mihai Liviu Cojocar on 04/05/2021.
//

import RxDataSources
import struct Core.CompanyInfo
import struct Core.Launch

enum MultipleSectionModel {
    case company(title: String, items: [SectionItem])
    case launches(title: String, items: [SectionItem])
    case loading(title: String, items: [SectionItem])
    
    func sortedLaunches(ascending: Bool, launchSuccessful: Bool?, launchYear: Int?) -> MultipleSectionModel {
        switch self {
        case .company, .loading: return self
        case let .launches(title: title, items: items):
            var sortedItems = items.filter { sectionItem -> Bool in
                if case let .launchSectionItem(launch: launch) = sectionItem {
                    if let launchSuccessful = launchSuccessful {
                        if let launchYear = launchYear {
                            return launch.launchSuccess == launchSuccessful && launch.date?.year() == launchYear
                        } else {
                            return launch.launchSuccess == launchSuccessful
                        }
                    } else if let launchYear = launchYear {
                        return launch.date?.year() == launchYear
                    } else {
                        return true
                    }
                } else {
                    return false
                }
            }
            
            sortedItems.sort { (lhs, rhs) -> Bool in
                if case let .launchSectionItem(launch: lhsLaunch) = lhs, case let .launchSectionItem(launch: rhsLaunch) = rhs {
                    if ascending {
                        return lhsLaunch.dateUnix < rhsLaunch.dateUnix
                    } else {
                        return lhsLaunch.dateUnix > rhsLaunch.dateUnix
                    }
                } else {
                    return false
                }
            }
            
            return .launches(title: title, items: sortedItems)
        }
    }
}

extension MultipleSectionModel: SectionModelType {
    typealias Item = SectionItem
    
    var items: [SectionItem] {
        switch self {
        case .company(title: _, items: let items):
            return items.map { $0 }
        case .launches(title: _, items: let items):
            return items.map { $0 }
        case .loading(title: _, items: let items):
            return items.map { $0 }
        }
    }
    
    init(original: MultipleSectionModel, items: [SectionItem]) {
        switch original {
        case let .company(title: title, items: _):
            self = .company(title: title, items: items)
        case let .launches(title: title, items: _):
            self = .launches(title: title, items: items)
        case let .loading(title: title, items: _):
            self = .loading(title: title, items: items)
        }
    }
}

extension MultipleSectionModel {
    var title: String {
        switch self {
        case let .company(title: title, items: _): return title
        case let .launches(title: title, items: _): return title
        case let .loading(title: title, items: _): return title
        }
    }
}

extension MultipleSectionModel {
    static func loadingSection(title: String) -> Self {
        return MultipleSectionModel.loading(title: title, items: [.loading])
    }
}
